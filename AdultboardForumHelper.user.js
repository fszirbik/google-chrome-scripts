// ==UserScript==
// @name         AdultboardForumHelper
// @namespace    fszirbik/google-chrome-scripts
// @version      0.4
// @description
// @author       Szife
// @match        https://www.adultboard.net/forums/*
// @downloadURL  https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/AdultboardForumHelper.user.js
// @updateURL    https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/AdultboardForumHelper.user.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
.preview-div {
  margin: 2px;
  display: flex;
  align-items: center;
}
.preview-image {
  max-width: 150px;
  max-height: 180px;
  margin-right: 2px;
`);

function download(url, handler) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                handler(xhr.responseText);
            }
        }
    }
    xhr.send();
}

const excludes = [
    'ClubKayden', 'BrazzersExxtra', 'MILFsLikeItBig', 'BigWetButts', 'BlackedRaw', 'BigButtsLikeItBig',
    'HandsOnHardcore', 'DDFBusty', 'AllOver30', 'TeenyBlack', 'BabyGotBoobs', 'HotAndMean', 'SuicideGirls',
    'BigTitsAtWork', 'BigTitsAtSchool', 'MommyGotBoobs', 'DirtyMasseur', 'MomsInControl', 'BigNaturals',
    'HotLegsAndFeet', 'OnlyBlowJob', 'TittyAttack', 'Only-Secretaries', 'OyeLoca', 'TeamSkeetSelects',
    'FamilyStrokes', 'VirtualTaboo', 'ATKExotics', 'ATKHairy', 'NHLPCentral', 'VintageFlash', 'AuntJudys',
    'FootFetishDaily', 'MelenaMariaRya', 'GarrysGirls', 'WildOnCam', 'Anilos', 'AlettaOcean', 'Catalina Cruz',
    'RonisParadise', 'OfficeErotic', 'ATKingdom', 'AlexisNaiomi', 'Cosmid', 'AMKingdom', 'WetAndPuffy',
    'TessaFowler', 'LadyLucy', 'PinupFiles', 'Scoreland', 'Nadine-J', 'KarupsOW', 'BillsHoneys', 'ATKArchives',
    'NudeX', 'SweetheartVideo', 'Bryci', 'AbbyWinters', 'FTVMilfs', 'GirlsDelta', 'MommysGirl', 'FallInLovia',
    'DeskBabes', 'OfficeFantasy', 'MeetMadden', 'College-Uniform', 'Only-Opaques', 'Blacked.com', 'deeper.com',
    'Tushy', 'DoctorAdventures', 'ShopLyfter', 'OnlyTease', 'BustyBritain', 'MatureEU', 'EliseErotic',
    'TheBlackAlley', 'Yanks'
];

(function() {
    'use strict';

    const blockBody = document.querySelector('[class="block-body"]');
    //console.warn(blockBody);
    const titleLinks = blockBody.querySelectorAll('a[data-preview-url]');
    //console.warn(titleLinks);
    titleLinks.forEach((link, linkIdx) => {
        //if(linkIdx > 0) return;

        if(excludes.find(e => link.parentNode.textContent.includes(e))) {
            link.parentNode.parentNode.parentNode.remove();
            return;
        }

        const labelLink = link.parentNode.querySelector('a[class="labelLink"]');
        if(labelLink) {
            //console.warn(labelLink.textContent);
            link.textContent = labelLink.textContent + ' - ' + link.textContent;
        }

        const previewDiv = link.parentNode.appendChild(document.createElement("div"));
        previewDiv.setAttribute("class", "preview-div");
        //previewDiv.setAttribute("style", "");

        download(link.href, (html) => {
            var doc = new DOMParser().parseFromString(html, "text/html");
            let images = doc.querySelectorAll('img[class="bbImage"]');

            if(images.length < 5) {
                images.forEach(imageLink => {
                    const image = previewDiv.appendChild(document.createElement("img"));
                    image.setAttribute("class", "preview-image");
                    image.src = imageLink.src;
                });
            } else {
                let step = Math.floor(images.length / 5);
                //console.warn("STEP: " + step);
                if(step > 0) {
                    let previewCnt = 0;
                    for(let stepCnt = 0; stepCnt < images.length; stepCnt+=step) {
                        let imageLink = images[stepCnt].src;
                        //console.warn(imageLink);
                        const image = previewDiv.appendChild(document.createElement("img"));
                        image.setAttribute("class", "preview-image");
                        image.src = imageLink;

                        if(++previewCnt >= 5) break;
                    }
                }
            }
        });

    });
})();