// ==UserScript==
// @name         ChatUBT
// @namespace    fszirbik/google-chrome-scripts
// @version      0.3
// @description
// @author       Szife
// @match        https://chaturbate.com/*/
// @icon         https://static-assets.highwebmedia.com/favicons/favicon.ico
// @grant        GM_addStyle
// @run-at       document-idle
// @downloadURL  https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/ChatUBT.user.js?inline=false
// @updateURL    https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/ChatUBT.user.js?inline=false
// ==/UserScript==

(function() {
    'use strict';

    window.setTimeout(() => {
        const body = document.getElementsByTagName("body").item(0);
        // console.warn(body);

        const videoMode = document.getElementById("video-mode");
        // console.warn(videoMode);
        if(!videoMode) {
            return;
        }

        const topSectionWrapper = document.querySelector(".topSectionWrapper")
        if(!topSectionWrapper) {
            return;
        }

        const videoPanel = topSectionWrapper.querySelector("#VideoPanel")
        if(!videoPanel) {
            return;
        }

        const resizeHandle = body.querySelector(".resizeHandle")
        if(resizeHandle) {
            resizeHandle.remove();
        }

        const topTab = body.querySelector('[data-paction="TopTab"][href="/?g=t"]');
        // console.warn("topTab"); console.warn(topTab);
        const genderTabs = body.querySelector('.genderTabs');
        // console.warn("genderTabs"); console.warn(genderTabs);
        if(topTab || genderTabs) {
            const followButton = body.querySelector(".followButton")
            // console.warn("followButton"); console.warn(followButton);
            if(followButton) {
                followButton.remove();
            }

            const unfollowButton = body.querySelector(".unfollowButton")
            // console.warn("unfollowButton"); console.warn(unfollowButton);
            if(unfollowButton) {
                unfollowButton.remove();
            }
            if(topTab) {
                topTab.parentElement.appendChild(followButton);
                topTab.parentElement.appendChild(unfollowButton);
            } else if(genderTabs) {
                genderTabs.firstElementChild.firstElementChild.appendChild(followButton);
                genderTabs.firstElementChild.firstElementChild.appendChild(unfollowButton);
            }

            GM_addStyle(".unfollowButton, .followButton { margin-top: 6px !important; }");

            let nextCamBgColor = body.querySelector(".nextCamBgColor")
            if(!nextCamBgColor) {
                nextCamBgColor = body.querySelector('[data-paction="NextCam"]');
            }
            if(nextCamBgColor) {
                nextCamBgColor.parentElement.remove();
            }
        }

        let width;
        let height;
        if( window.innerWidth )
        {
            width = window.innerWidth;
            height = window.innerHeight;
        } else {
            width = document.body.clientWidth;
            height = document.body.clientHeight;
        }
        //console.warn("size: " + width + ":" + height);
        height = width * 9 / 16;

        const player = document.getElementById("TheaterModePlayer");
        player.remove();
        // console.warn(player);
        player.setAttribute("style", player.getAttribute("style") + " min-height:" + height + "px; height:" + height + "px;");

        const video = player.querySelector("video");
        video.setAttribute("style", video.getAttribute("style") + "object-position: top;");

        topSectionWrapper.insertBefore(player, videoPanel);

        if(genderTabs) {
            genderTabs.scrollIntoView();
        } else {
            player.scrollIntoView();
        }

        const chat = document.querySelector("#ChatTabContainer");
        if(chat) {
            chat.setAttribute("style", chat.getAttribute("style") + "display: block; position: relative; min-width: 100%;");
            chat.remove();
            topSectionWrapper.insertBefore(chat, videoPanel);
        }

        return;
    }, 230);

})();
