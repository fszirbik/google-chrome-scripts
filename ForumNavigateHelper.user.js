// ==UserScript==
// @name        ForumNavigateHelper
// @namespace   fszirbik/google-chrome-scripts
// @description 
// @match       https://vipergirls.to/threads/*
// @downloadURL https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/ForumNavigateHelper.user.js
// @updateURL   https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/ForumNavigateHelper.user.js
// @version     0.1
// @grant       none
// @run-at      document-end
// @require     https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js
// ==/UserScript== 

var min_image_count = 10;
var divs = document.getElementsByTagName('BLOCKQUOTE');
for (var i = 0; i < divs.length; i++) {
  var div = divs[i];
  var imgs = div.getElementsByTagName('IMG');
  if (imgs.length > 8) {
    var brs = div.getElementsByTagName('BR');
    console.log('BRS');
    console.log(brs);
    for (var j = 0; j < brs.length; j++) {
      if (brs[j].previousSibling === undefined || brs[j].previousSibling === null || brs[j].previousSibling.nodeName === 'img') {
        brs[j].setAttribute('hidden', 'hidden');
      }
    }
  }
  for (var j = 0; j < imgs.length; j++) {
    var img = imgs[j];
    //console.log(img.src);
    img.name = 'image_' + i;
    img.setAttribute('data-src', img.src);
    if (imgs.length > min_image_count) {
      var num = Math.floor(j % (imgs.length / min_image_count));
      if (num !== 0) {
        img.setAttribute('hidden', 'hidden');
        img.setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs='); /*empty GIF*/
        //img.setAttribute('src', '');
      }
    }
  }
  if (imgs.length > min_image_count) {
    div.appendChild(document.createElement('br'));
    var allButton = document.createElement('button');
    allButton.setAttribute('value', 'MIND');
    allButton.setAttribute('data-name', 'image_' + i);
    allButton.innerHTML = 'ALL';
    div.appendChild(allButton);
    allButton.addEventListener('click', function (event) {
      //console.log("CLICK");      
      //console.log(this);
      var imgs = document.getElementsByName(this.getAttribute('data-name'));
      console.log(imgs);
      for (var k = 0; k < imgs.length; k++) {
        var img = imgs[k];
        img.removeAttribute('hidden');
        img.setAttribute('src', img.getAttribute('data-src'));
      }
    });
  }
}
var divs = document.getElementsByTagName('DIV');
var naviDiv = null;
for (var i = 0; i < divs.length; i++) {
  var clazz = divs[i].getAttribute('class');
  // console.log(clazz + '\n');
  if (clazz !== null && clazz !== undefined) {
    if (naviDiv === null && clazz.indexOf('navlinks') >= 0) {
      naviDiv = divs[i];
      break;
    }
  }
}

var clipboardButton = document.createElement('button');
clipboardButton.setAttribute('value', 'clipboard');
clipboardButton.setAttribute("class", "clipboardButton");
//clipboardButton.setAttribute("hidden", "true");
document.getElementsByTagName("body")[0].appendChild(clipboardButton);

new Clipboard('.clipboardButton', {
    text: function() {
            console.log( window.location.href );
        return window.location.href;
    }
});

if (naviDiv !== null) {
  //console.log( naviDiv );
  var aTags = naviDiv.getElementsByTagName('A');
  var leftA = aTags[0];
  var rightA = aTags[1];
  if (leftA !== null && leftA !== undefined) {
    document.onkeydown = function (evt) {
      evt = evt || window.event;
      console.log(evt);
      console.log(evt.ctrlKey);
      console.log(evt.keyCode === 37);
      if (evt.ctrlKey === true && evt.keyCode === 37) { // LEFT
        console.log(leftA.getAttribute('href'));
        location.assign(leftA);
      }
      if (evt.ctrlKey === true && evt.keyCode === 39) { // RIGTH
        console.log(rightA.getAttribute('href'));
        location.assign(rightA);
      }
      if (evt.ctrlKey === true && evt.keyCode === 13) { // ENTER
        if(Clipboard.isSupported()) {
            clipboardButton.click();
        } else {
            console.log( "Clipboard not supported!" );
        }
      }
    };
  }
}
function scroll() {
  var scrollLength = 400;
  var premiumNotice = document.getElementById('navbar_notice_219');
  if (premiumNotice === null) {
    scrollLength = scrollLength - 155;
  }
  if (window.pageYOffset < scrollLength) {
    window.scrollTo(0, scrollLength);
  }
}
scroll();
window.onload = function () {
  scroll();
}; 