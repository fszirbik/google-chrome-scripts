// ==UserScript==
// @name         KittyKatsForumBrowser
// @namespace    fszirbik/google-chrome-scripts
// @version      0.4
// @description
// @author       SzF
// @match        https://kitty-kats.net/forums/*
// @downloadURL  https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/KittyKatsForumBrowser.user.js
// @updateURL    https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/KittyKatsForumBrowser.user.js
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';

    const excludes = [
        'ClubKayden', 'BrazzersExxtra', 'MILFsLikeItBig', 'BigWetButts', 'BlackedRaw', 'BigButtsLikeItBig',
        'HandsOnHardcore', 'DDFBusty', 'AllOver30', 'TeenyBlack', 'BabyGotBoobs', 'HotAndMean', 'SuicideGirls',
        'BigTitsAtWork', 'BigTitsAtSchool', 'MommyGotBoobs', 'DirtyMasseur', 'MomsInControl', 'BigNaturals',
        'HotLegsAndFeet', 'OnlyBlowJob', 'TittyAttack', 'Only-Secretaries', 'OyeLoca', 'TeamSkeetSelects',
        'FamilyStrokes', 'VirtualTaboo', 'ATKExotics', 'ATKHairy', 'NHLPCentral', 'VintageFlash', 'AuntJudys',
        'FootFetishDaily', 'MelenaMariaRya', 'GarrysGirls', 'WildOnCam', 'Anilos', 'AlettaOcean', 'Catalina Cruz',
        'RonisParadise', 'OfficeErotic', 'ATKingdom', 'AlexisNaiomi', 'Cosmid', 'AMKingdom', 'WetAndPuffy',
        'TessaFowler', 'LadyLucy', 'PinupFiles', 'Scoreland', 'Nadine-J', 'KarupsOW', 'BillsHoneys', 'ATKArchives',
        'NudeX', 'SweetheartVideo', 'Bryci', 'AbbyWinters', 'FTVMilfs', 'GirlsDelta', 'MommysGirl', 'FallInLovia',
        'DeskBabes', 'OfficeFantasy', 'MeetMadden', 'College-Uniform', 'Only-Opaques', 'Blacked.com', 'deeper.com',
        'Tushy', 'DoctorAdventures', 'ShopLyfter', 'OnlyTease', 'BustyBritain', 'MatureEU', 'EliseErotic',
        'TheBlackAlley', 'Yanks', 'XLGirls', 'MYLF', 'SexyVanessa', 'SkinTightGlamour', 'Southern-Charms', 'OnlyFans',
        'Petites-Parisiennes', 'NylonFeetParty', 'IStripper', 'SarennasWorld', 'Tiffany-Towers', 'BigTitTerryNova',
        'BustySammieBlack', 'MegaTitsMinka', 'NicolePeters', 'LinseysWorld', 'BustyLornaMorgan', 'BustyKellyKay',
        'KarinaHart', 'JuliaMiles', 'JessicaTurner', 'BustyKerrieMarie', 'BustyInesCudna', 'ChloesWorld', 'BustyMerilyn',
        'BustyDustyStash', 'BustyAngelique', 'Autumn-Jade', 'BustyArianna', 'Pol-Legs', 'BrookieLittle', 'ScoreClassics',
        'HosieryHome', 'ATKGirlfriends', 'IShotMyself', 'Plushies', 'BustyBrits', 'RobynJames', '40SomethingMag',
        'ArtOfGloss', 'GirlsOutWest', 'Red-XXX', 'GlossTightsGlamour', 'StaceyPoole', '40ozBounce', 'Dylan Ryder',
        'HollyRandall', 'AliceBrookes', 'HayleysSecrets', 'PriyaRai', 'SophieRose', 'LovePop', 'Digi-Gra', 'Cosdoki',
        '50PlusMILFs', 'StrictlyGlamour', 'MatureNL', 'Plumperpass', 'Art-Lingerie', 'PaulRaymond', 'Graphis',
        'PantyhoseAddict', 'MelissaDebling', 'LanaParker', 'MilenaAngel', 'OfficialSammyBraddy', 'ChelseaOfficial',
        'AdriannePantyhose', 'SpinChix', 'NylonGlamour', 'CherryPimps', 'Secretease', 'Lady-Sonia', 'ArtOfLegs',
        'BustyAlli', 'LadyInNylons', 'Layered-Nylons', 'CutiesInTights', 'BukkakeFest', 'SaraJay', 'PantyhoseLane',
        'StockingAces', 'RosieJones', 'WeAreHairy', 'OnlySilkAndSatin', 'BabesInNylons', 'NylonStockingSluts',
        'MoreThanNylons', 'DivineBreasts', 'AlaInPantyhose', 'MexicanLust', 'BikiniRiot', 'StMackenzies',
        'AmateurAdrianne', 'BootyLiciousMag', 'OlderWomanFun', 'TokyoDoll', 'PlumpMature', 'PinupGlamour',
        "Playboy"
    ];


    const htmlTag = document.getElementsByTagName("html")[0];
    const xfToken = htmlTag.getAttribute("data-csrf")
    //console.warn(xfToken);

    const forums = document.querySelectorAll(".structItem--thread");
    for(let i = 0; i<forums.length; i++) {
        const forum = forums.item(i);
        const title = forum.querySelector(".structItem-title");
        const found = excludes.find(ex => title.textContent.toLowerCase().includes(ex.toLowerCase()));
        if(found) {
            forum.remove();
        }
    }

    const items = document.getElementsByClassName("structItem-cell--main");
    //console.warn("ITEMS");
    //console.warn(items);

    for(let i=0; i<items.length; i++) {
        const item = items[i];
        //console.warn(item);

        const toolTipDiv = item.querySelector('[data-xf-init="preview-tooltip"]');
        if(!toolTipDiv) continue;

        const prevUrl = toolTipDiv.getAttribute("data-preview-url");
        //console.warn(prevUrl);

        const xhttp = new XMLHttpRequest();
        xhttp.open("GET", prevUrl + "?_xfToken=" + xfToken + "&_xfResponseType=json");
        xhttp.addEventListener("load", event => {
            //console.warn("LOADED: " + prevUrl);
            //console.warn(xhttp.responseText);

            const respData = JSON.parse(xhttp.responseText);
            //console.warn(respData);
            if(respData && respData.status === "ok" && respData.html && respData.html.content) {
                //console.warn(respData.html.content);
                const newNode = document.createElement("div");
                newNode.innerHTML = respData.html.content;
                //console.warn(newNode);
                item.appendChild(newNode);
                newNode.setAttribute("hidden", true);

                let images = newNode.querySelectorAll("img.bbImage");
                const tnCnt = 5;
                let inc = images.length < tnCnt ? 1 : images.length / tnCnt;
                // console.warn(images.length + "; " +inc);
                for(let i=0; i<images.length; i = (inc == 0 ? i+1 : i+inc)) {
                    let img = images.item(i);
                    img.setAttribute("style", "max-width: 120px");
                    item.appendChild(img);
                }

                newNode.remove();
            }
        });
        xhttp.send();
    }

    GM_addStyle(".bbImage { max-height: 180px; margin-right: 3px;} ");
})();
