// ==UserScript==
// @name         TorrentGalaxy Helper
// @namespace    fszirbik/google-chrome-scripts
// @version      1.0.0
// @description  try to take over the world!
// @author       You
// @match        https://torrentgalaxy.to/torrents.php*
// @match        https://torrentgalaxy.mx/torrents.php*
// @match        https://tgx.rs/torrents.php*
// @match        https://tgx.sb//torrents.php*
// @match        https://torrentgalaxy.mx/torrents.php?*
// @match        https://torrentgalaxy.to/torrents.php?*c41=*
// @match        https://torrentgalaxy.to/torrents.php?*cat=41*
// @match        https://torrentgalaxy.to/torrents.php?*c5=*
// @match        https://torrentgalaxy.to/torrents.php?*cat=5*
// @match        https://torrentgalaxy.to/torrents.php?*c6=*
// @match        https://torrentgalaxy.to/torrents.php?*cat=6*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=torrentgalaxy.to
// @require      https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const excludes = [
        "tt31849826", // Doctor.Odyssey.S01E01.720p.HDTV.x264-SYNCOPY[TGx]
        "tt15121038", // Everybody.Still.Hates.Chris.S01E02.720p.WEB.x265-MiNX[TGx]
        "tt31458276", // Grotesquerie S01E01 720p DSNP WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt12408718", // Murder in a Small Town 2024 S01E01 The Suspect 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt9128874", // Van der Valk 2020 S04E02 Safe in Amsterdam Part 2 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt33343774", // Cabin in the Woods S01E01 Desert Manhunt 720p AMZN WEB-DL DDP2 0 H 264-Kitsune[TGx]
        "tt7278862", // My.Brilliant.Friend.S04E02.SUBBED.720p.WEB.H264-SKYFiRE[TGx]
        "tt26748649", // High Potential S01E02 Dancers In The Dark 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt26448526", // Brilliant Minds S01E01 Pilot 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt10323338", // 9-1-1.Lone.Star.S05E01.720p.HDTV.x265-MiNX[TGx]
        "tt26591147", // Matlock 2024 S01E01 Pilot 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt27629382", // Rescue.HI-Surf.S01E01.720p.HDTV.x265-MiNX[TGx]
        "tt3780132", // Bachelor in Paradise S04E02 720p HULU WEB-DL AAC2 0 H 264-NTb[TGx]
        "tt4305162", // Expedition.Unknown.S13E06.Americas.MIA.Heroines.720p.WEB.h264-CBFM[TGx]
        "tt3612488", // The.Noite.com.Danilo.Gentili.2024.09.20.720p.WEB.x264-Skylane77.mp47
        "tt0328290", // The Project (BBC, 2002) S01 complete (1280x720p HD, 50fps, soft Eng subs)
        "tt14124236", // Frasier 2023 S02E01 Ham 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt8425404", // The.Bay.S05E02.720p.WEB.H264-DiMEPiECE[TGx]
        "tt8888462", // Pachinko.S02E04.720p.WEB.x265-MiNX[TGx]
        "tt8041658", // Marvels Moon Girl and Devil Dinosaur S01E08 Teachers Pet 720p HULU WEB-DL DDP5 1 H 264-NTb...
        "tt15571732", // Agatha All Along S01E02 Circle Sewn With Fate Unlock Thy Hidden Gate 720p DSNP WEB-DL DDP5...
        "tt13836422", // Secret.Celebrity.Renovation.S03E00.Mookie.Betts.720p.WEB.h264-EDITH[TGx]
        "tt4934214", // Taskmaster S18E02 And Then a Detective Comes In 720p ALL4 WEB-DL AAC2 0 H 264-RAWR[TGx]
        "tt27950663", // The.Marlow.Murder.Club.S01E01.720p.WEB.H264-SKYFiRE[TGx]
        "tt27774673", // High.Potential.S01E01.720p.HDTV.x265-MiNX[TGx]
        "tt0463398", // Dancing.With.The.Stars.US.S33E01.720p.WEB.h264-EDITH[TGx]
        "tt8634332", // The Righteous Gemstones S01E09 Better is the End of a Thing Than Its Beginning 720p AMZN W...
        "tt14321914", // The.Chelsea.Detective.S01E05.720p.WEB.H264-SKYFiRE[TGx]
        "tt2216156", // Last.Tango.In.Halifax.S01E06.720p.WEB.H264-SKYFiRE[TGx]
        "tt23730262", // How.To.Die.Alone.S01E01.720p.x265-TiPEX [Saturn5]
        "tt5712554", // The Grand Tour S05E03 Sand Job 720p AMZN WEB-DL DDP5 1 H 264-playWEB[TGx]
        "tt11947264", // The Astronauts S01E08 720p AMZN WEB-DL DDP2 0 H 264-NTb[TGx]
        "tt33038598", // Cursed.Gold.A.Shipwreck.Scandal.S01E02.720p.HDTV.x264-skorpion.mp4
        "tt13994572", // Sherwood 2022 S02E06 720p iP WEB-DL AAC2 0 HFR H 264-RAWR[TGx]
        "tt23469464", // Universal.Basic.Guys.S01E01.720p.x265-TiPEX [Saturn5]
        "tt30644285", // Big Cats 24/7 S01E03 WEB-DL (720p)
        "tt30428188", // Fight.Night.The.Million.Dollar.Heist.S01E03.720p.x265-TiPEX [Saturn5]
        "tt13018148", // Tell.Me.Lies.S02E02.720p.x265-TiPEX [Saturn5]
        "tt5875444", // Slow.Horses.S04E01.720p.x265-TiPEX [Saturn5]
        "tt20782190", // English.Teacher.S01E01.720p.x265-TiPEX [Saturn5]
        "tt2675934", // Gold Rush The Dirt S07E08 720p AMZN WEB-DL DDP2 0 H 264-NTb[TGx]
        "tt18767204", // Last.King.of.the.Cross.S02E01.720p.x265-TiPEX [Saturn5]
        "tt7631058", // The.Lord.of.the.Rings.The.Rings.of.Power.S02E03.REPACK.720p.x265-TiPEX [Saturn5]
        "tt12851524", // Only.Murders.in.the.Building.S04E01.720p.x265-TiPEX [Saturn5]
        "tt11691774", // Only Murders in the Building S04E01 Once Upon a Time in the West 720p DSNP WEB-DL DDP5 1 H...
        "tt1834220", // Dam Busters Declassified (BBC, 2010) (1280x720p HD, 50fps, soft Eng subs)
        "tt30005404", // Stags.2024.S01E03.720p.x265-TiPEX [Saturn5]
        "tt14627480", // Evil S04E06 How To Dance In Three Easy Steps 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt11231226", // Reasonable.Doubt.2022.S02E01.720p.WEB.x265-MiNX[TGx]
        "tt12138676", // Celebrity IOU S08E02 Wanda Sykes Razes The Roof 720p DISC WEB-DL AAC2 0 H 264-DoGSO[TGx]
        "tt22035106", // Gem.Hunters.Down.Under.S03E06.Light.at.the.End.of.the.Tunnel.720p.WEBRip.x264-skorpion.mp4
        "tt28466465", // Rick.and.Morty.The.Anime.S01E01.720p.x265-TiPEX [Saturn5]
        "tt12878838", // Bel-Air S03E01 Baby Im Back 720p PCOK WEB-DL DDP5 1 x264-NTb[TGx]
        "tt15203646", // Bad Monkey S01E01 The Floating-Human-Body-Parts Capital of America 720p ATVP WEB-DL DDP5 1...
        "tt1928307", // Time.Bandits.S01E08.720p.x265-TiPEX [Saturn5]
        "tt7671070", // Industry.S03E01.720p.x265-TiPEX [Saturn5]
        "tt32152432", // Gladiators.2024.S01E04.720p.x265-TiPEX
        "tt9813792", // From S01E08 Broken Windows Open Doors 720p AMZN WEB-DL DDP5 1 H 264-TEPES[TGx]
        "tt33030134", // Cowboy.Cartel.S01E03.720p.x265-TiPEX [Saturn5]
        "tt0149460", // Futurama S09E01 The One Amigo 720p DSNP WEB-DL DD 5 1 H 264-playWEB[TGx]
        "tt4770018", // Love Island S11E55 720p ITVX WEB-DL AAC2 0 x264-WhiteHat[TGx]
        "tt3747572", // Grantchester S09E05 Episode 5 720p AMZN WEB-DL DDP5 1 H 264-MADSKY[TGx]
        "tt19853518", // Kite.Man.Hell.Yeah.S01E03.720p.x265-TiPEX
        "tt14022668", // Lady in the Lake S01E02 It has to do with the search for the marvelous 720p ATVP WEB-DL DD...
        "tt0963967", // Bondi.Rescue.S18E01.720p.WEB-DL.AAC2.0.H.264-WH.mkv
        "tt18070898", // Sunny S01E01 Hes in Refrigerators 720p ATVP WEB-DL DDP5 1 Atmos H 264-FLUX[TGx]
        "tt14022350", // The Serpent Queen S02E01 Grand Tour 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt27601690", // Emperor.of.Ocean.Park.S01E01.720p.x265-TiPEX [Saturn5]
        "tt0348894", // The Bachelorette S21E01 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt0251497", // Big Brother US S16E03 720p HDTV x264-2HD-Obfuscated[TGx]
        "tt14088994", // All.American.Homecoming.S03E01.720p.x265-TiPEX [Saturn5]
        "tt10589968", // The Secret of Skinwalker Ranch S05E07 The Cone Zone 720p HULU WEB-DL AAC2 0 H264-WhiteHat[...
        "tt7422238", // Classic Albums Rush 2112, Moving Pictures (2010 720p-WEB).mp4
        "tt32214529", // Sins.of.the.South.S01E08.720p.WEBRip.x264-BAE[TGx]
        "tt0759364", // Americas Got Talent S12E10 Judge Cuts 3 720p HULU WEB-DL AAC2 0 H 264-NTb[TGx]
        "tt3069720", // The.Amazing.Race.Canada.S10E01.720p.WEB.h264-BAE[TGx]
        "tt1587934", // American Ninja Warrior S12E03 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt14256490", // Secrets.Of.The.London.Underground.S04E01.720p.WEBRip.x264-skorpion.mp4
        "tt9170070", // 90 Day Fiance The Other Way S06E01 My Best Friends Meddling 720p AMZN WEB-DL DDP2 0 H 264-...
        "tt11639300", // Grace.2021.S04E01.720p.WEB.x265-MiNX[TGx]
        "tt27733069", // Beyond.Skinwalker.Ranch.S02E04.Ancient.Dimensions.720p.Web-DL.AAC2.0.H264-DonJuan[TGx]
        "tt32123951", // Hollywood Con Queen S01E03 Thank You for the Story 720p ATVP WEB-DL DDP5 1 Atmos H 264-FLU...
        "tt4803766", // Alone S11E02 Opportunity Cost 720p AMZN WEB-DL DDP2 0 H 264-FLUX[TGx]
        "tt2191567", // Wicked Tuna S11E02 720p DSNP WEB-DL DD5 1 H 264-NTb[TGx]
        "tt13821126", // Extrapolations.S01E05.720p.WEB.x265-MiNX[TGx]
        "tt4791250", // Naked.And.Afraid.XL.S10E06.A.Camp.Divided.720p.WEB.h264-CBFM[TGx]
        "tt11198330", // House.Of.The.Dragon.S02E02.720p.WEB.x265-MiNX[TGx]
        "tt26742875", // Mean Girl Murders S01E04 Desperate Texas Housewives 720p MAX WEB-DL DD 2 0 H 264-playWEB[T...
        "tt1849622", // Tanked S08E11 Tank You Come Again! 720p WEBRip X264-NGP[TGx]
        "tt5595238", // Tanked S11E01 720p AMZN WEBRip DD2 0 x264-NTb[TGx]
        "tt3052478", // Masters of Illusion S03E10 Body Piercings Mind Over Matter and Those Crazy Cards 720p CW W...
        "tt7845644", // Single Parents S01E08 The Beast 720p DSNP WEB-DL DD 5 1 H 264-playWEB[TGx]
        "tt8543208", // Looney.Tunes.Cartoons.S06E10.720p.WEB.h264-DOLORES[TGx]
        "tt17676706", // Hotel Cocaine S01E01 The Mutiny 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt1442437", // Modern.Family.S02E08.720p.WEB.x265-MiNX[TGx]
        "tt4354880", // Mr Mercedes S02E01 Missed You 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt8819906", // Love Island US S06E01 720p HDTV x264-NGP[TGx]
        "tt0445912", // The.Ultimate.Fighter.S32E02.720p.WEB-DL.H264.Fight-BB
        "tt17677860", // Presumed.Innocent.S01E02.720p.x265-TiPEX[TGx]
        "tt15475330", // Black Cake S01E04 Mrs Bennett 720p DSNP WEB-DL DD 5 1 H 264-playWEB[TGx]
        "tt14527610", // Harry Wild S03E03 720p WEB x264-NGP[TGx]
        "tt18182112", // Fantasmas S01E01 Cookies and Spaghetti 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt10732104", // Power.Book.II.Ghost.S04E01.720p.WEB.x265-MiNX[TGx]
        "tt6917254", // Beat.Shazam.S07E01.720p.WEB.h264-BAE[TGx]
        "tt0446809", // Deadliest Catch S00E83 The 20th Anniversary 720p AMZN WEB-DL DDP2 0 H 264-NTb[TGx]
        "tt19380952", // Clipped 2024 S01E02 A Blessing and a Curse 720p DSNP WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt10087444", // Below Deck Sailing Yacht S02E11 Crash Boom Bang! 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt5564124", // 60.Days.In.S09E01.720p.WEB.H264-BeechyBoy[TGx]
        "tt14586544", // Billy.the.Kid.2022.S02E05.720p.x264-FENiX [Saturn5]
        "tt11712058", // Mayor.of.Kingstown.S03E01.720p.x264-FENiX [Saturn5]
        "tt6639066", // Gold Rush Parkers Trail S07E01 River of Never-Ending Gold 720p AMZN WEB-DL DDP2 0 H 264-NT...
        "tt1694423", // MasterChef.US.S14E01.720p.WEB.h264-BAE[TGx]
        "tt27349153", // Insomnia.2024.S01E01.720p.x265-T0PAZ [Saturn5]
        "tt9198016", // Big Cat Tales S01E05 Survivors of the Savanna 720p MAX WEB-DL DD 2 0 H 264-playWEB[TGx]
        "tt11685912", // Outer.Range.S02E06.720p.WEB.x265-MiNX[TGx]
        "tt0121955", // South Park S00E49 South Park The End of Obesity 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt6294706", // The.Chi.S06E11.720p.WEB.x265-MiNX[TGx]
        "tt19399602", // The.Big.Cigar.S01E03.720p.WEB.x265-MiNX[TGx]
        "tt10516496", // Chip.n.Dale.Park.Life.S02E16.720p.WEB.H264-SHIIIT[TGx]
        "tt9055008", // Evil S04E01 How to Split an Atom 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt15791630", // The Kardashians S05E01 Welcome to My Mind 720p DSNP WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt10982034", // Trying S04E02 Ghosting 720p ATVP WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt13075042", // Pretty.Little.Liars.Original.Sin.S02E04.720p.x265-T0PAZ [Saturn5]
        "tt15180910", // Reginald.the.Vampire.S02E03.720p.HDTV.x264-SYNCOPY[TGx]
        "tt5699846", // Declassified.Untold.Stories.of.American.Spies.S03E08.Operation.Firewall.720p.WEBRip.x264-C...
        "tt10327354", // Prodigal Son S02E01 Its All In The Execution 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt13875494", // Tracker 2024 S01E12 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt1411598", // The Real Housewives of New Jersey S14E02 The Icing on the Brain Cake 720p AMZN WEB-DL DDP2...
        "tt14921986", // Interview.with.the.Vampire.S02E01.720p.x265-T0PAZ [Saturn5]
        "tt1497563", // Hoarders S15E97 Where Are They Now John Andy 720p HULU WEB-DL AAC2 0 H 264-NTb[TGx]
        "tt19231492", // Dark.Matter.2024.S01E03.720p.WEB.x265-MiNX[TGx]
        "tt2674806", // Inside No.9 S09E01 Boo to a Goose (1280x720p HD, 50fps, soft Eng subs)
        "tt18249282", // Shoresy S01E02 Veteran Presence 720p CRAV WEBRip DD5 1 H264-NTb[TGx]
        "tt13438882", // Celebrity.Wheel.of.Fortune.S04E10.720p.WEB.h264-EDITH[TGx]
        "tt4299972", // The Jinx The Life and Deaths of Robert Durst S02E04 The Unluckiest Man In The World 720p H...
        "tt2498968", // Catfish.The.TV.Show.S09E05.720p.WEB.h264-BAE[TGx]
        "tt1769411", // Accused S02E03 Stephens Story 720p WEB x264-NGP[TGx]
        "tt14674086", // Welcome to Wrexham S03E02 Goals 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt10541088", // Clarksons.Farm.S03E01.720p.WEB.x265-MiNX[TGx]
        "tt1034201", // Britains Got Talent S17E03 Auditions 3 720p STV WEB-DL AAC2 0 H 264-NGP[TGx]
        "tt11815682", // Hacks.S03E01.720p.WEB.x265-MiNX[TGx]
        "tt27390563", // Shardlake.S01E02.720p.x264-FENiX [Saturn5]
        "tt14669850", // neXt 2020 S01E10 FILE 10 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt9159144", // Dark Side of the Ring S05E03 720p CRAV WEB-DL DD5 1 H 264-NTb[TGx]
        "tt9159144", // Dark Side of the Ring S05E03 720p CRAV WEB-DL DD5 1 H 264-NTb[TGx]
        "tt13567344", // Acapulco.S03E01.720p.WEB.x265-MiNX[TGx]
        "tt21433150", // The.Veil.2024.S01E02.720p.x265-T0PAZ [Saturn5]
        "tt3640276", // The Brokenwood Mysteries S10E01 720p WEB-DL H264-NGP[TGx]
        "tt21376524", // Animal.Control.S02E07.720p.HDTV.x264-SYNCOPY[TGx]
        "tt14761860", // The.Big.Door.Prize.S02E03.720p.WEB.x265-MiNX[TGx]
        "tt26351130", // Secrets.of.the.Octopus.S01E01.720p.x264-FENiX [Saturn5]
        "tt1433870", // MasterChef.Australia.S16E01.720p.HDTV.x264-ORENJI[TGx]
        "tt28037356", // Opal.Hunters.Red.Dirt.Road.Trip.S02E01.720p.WEBRip.x264-skorpion.mp4
        "tt14404618", // The Sympathizer S01E02 Good Little Asian 720p AMZN WEB-DL DDP5 1 Atmos H 264-FLUX[TGx]
        "tt21064598", // Sullivans.Crossing.S02E02.720p.HDTV.x264-SYNCOPY[TGx]
        "tt10584446", // My.Life.Is.Murder.S04E01.720p.WEB.H264-ROPATA[TGx]
        "tt1640719", // Under.the.Bridge.S01E02.720p.x265-T0PAZ [Saturn5]
        "tt4248510", // Wicked Tuna Outer Banks S08E16 Red Seas 720p DSNP WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt11469796", // Kabaddi Khel India Ka (2016) 720p - Nat Geo India Documentary
        "tt0319931", // American.Idol.S22E10.720p.WEB.h264-EDITH[TGx]
        "tt9174724", // Beacon 23 S02E02 Purgatory 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt6685272", // The.Repair.Shop.S13E01.720p.WEBRip.x264-skorpion.mp4
        "tt21275092", // The Spencer Sisters S01E01 The Scholars Snafu 720p AMZN WEB-DL DD5 1 H 264-NTb[TGx]
        "tt18351584", // Franklin.2024.S01E03.720p.x264-FENiX [Saturn5]
        "tt8388390", // Chucky.S03E05.720p.x264-FENiX [Saturn5]
        "tt11306366", // Tyler Perrys Ruthless S04E22 Fistful of Greed 720p AMZN WEB-DL DDP2 0 H 264-MADSKY[TGx]
        "tt5396394", // American Housewife S05E02 Psych 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt8610082", // Monsters.at.Work.S02E02.720p.x265-T0PAZ [Saturn5]
        "tt15210156", // The Regime S01E06 Dont Yet Rejoice 720p HMAX WEB-DL DD5 1 H 264-playWEB[TGx]
        "tt27999118", // Vanderpump Villa S01E04 A Naughty Night in Versailles 720p DSNP WEB-DL DDP5 1 H 264-NTb[TG...
        "tt10048342", // The Queens Gambit S01E07 End Game 720p NF WEB-DL DDP5 1 x264-NTb[TGx]
        "tt6524350", // Big Mouth S04E10 What Are You Gonna Do 720p NF WEB-DL DDP5 1 x264-NTb[TGx]
        "tt18552362", // Parish.S01E02.720p.WEB.x265-MiNX[TGx]
        "tt14534600", // Dinosaur S01E03 720p DSNP WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt1636691", // Generator Rex S01E21 Payback 720p AMZN WEB-DL DDP2 0 H 264-NTb[TGx]
        "tt26591110", // Elsbeth S01E03 Reality Shock 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt1844624", // American Horror Story S12E06 Opening Night 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt5071412", // Ozark S01E06 Book Of Ruth 720p NF WEB-DL DD5 1 x264-NTb[TGx]
        "tt11006642", // Walker.S04E01.720p.HDTV.x265-MiNX[TGx]
        "tt14852960", // Lopez.vs.Lopez.S02E01.720p.x264-FENiX [Saturn5]
        "tt10329042", // All Rise S02E03 Sliding Floors 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt14271498", // Loot S02E02 Clueless 720p ATVP WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt12688394", // Weakest.Link.2020.S03E10.720p.WEB.h264-EDITH[TGx]
        "tt11188682", // The.Cleaning.Lady.S03E05.720p.HDTV.x264-SYNCOPY[TGx]
        "tt7414406", // All.American.S06E01.720p.x264-FENiX [Saturn5]
        "tt31862552", // Shed.and.Buried.Classic.Cars.S01E02.Mini.720p.WEBRip.x264-skorpion.mp4
        "tt1361787", // What Would You Do - S16E02 (720p).mkv
        "tt0367279", // Arrested Development S05E07 Rom-Traum 720p NF WEBRip DD5 1 x264-NTb[TGx]
        "tt12397680", // Fraggle Rock Back to the Rock S02E01 The Great Wind 720p ATVP WEB-DL DDP5 1 Atmos H 264-FL...
        "tt9788012", // Traces.S02E01.720p.HDTV.x264-ORGANiC[TGx]
        "tt14502758", // Renegade Nell S01E01 Dont Call Me Nelly 720p DSNP WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt8230448", // A.Gentleman.in.Moscow.S01E01.720p.x264-FENiX [Saturn5]
        "tt10313176", // Nancy.Drew.2019.S04E01.REPACK.720p.WEB.h264-ELEANOR[TGx]
        "tt7018644", // Grown-ish.S06E10.720p.x265-T0PAZ [Saturn5]
        "tt7949624", // The Baxters 2024 S01E01 Under the Surface 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt1532495", // American.Rust.S02E09.720p.WEB.x265-MiNX[TGx]
        "tt23181388", // Life on Our Planet S01E06 Chapter 6 Out of the Ashes 720p NF WEB-DL DDP5 1 Atmos H 264-FLU...
        "tt15399640", // Lawmen Bass Reeves S01E01 Part I REPACK 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt12351448", // The Reagans S01E02 The Right Turn 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt9114512", // We Were The Lucky Ones S01E03 Siberia 720p DSNP WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt13263106", // Murder on Middle Beach S01E02 Tables and Rooms 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt9233830", // Island Of Bryan S03E05 720p AMZN WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt10265028", // Off the Cuff S02E04 The Gamers Saving the World Citizen Science 720p AMZN WEB-DL DDP2 0 H ...
        "tt5969074", // Naked Attraction S07E01 Brian and Shaida 720p ALL4 WEB-DL AAC2 0 x264-NTb[TGx]
        "tt5480646", // Zombie House Flipping S05E13 Tampa Grace 720p AMZN WEB-DL DDP2 0 H 264-NTb[TGx]
        "tt3007640", // Naked.and.Afraid.S17E04.720p.WEB.h264-CBFM[TGx]
        "tt21266652", // Platform 7 S01E01 Episode 1 720p STAN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt0260618", // Gardeners' World S57E02 (2024) (720p, soft English subtitles)
        "tt21161156", // Beyond.Paradise.S02E01.720p.HDTV.x264-ORGANiC[TGx]
        "tt0285335", // The.Amazing.Race.S36E02.720p.HDTV.x264-SYNCOPY[TGx]
        "tt8888540", // Palm.Royale.S01E03.720p.WEB.x265-MiNX[TGx]
        "tt28082724", // Davey and Jonesies Locker S01E08 Schneiderverse 720p AMZN WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt0765425", // Top Chef S21E01 Chefs Test 720p AMZN WEB-DL DDP2 0 H 264-NTb[TGx]
        "tt0472023", // So.You.Think.You.Can.Dance.S18E03.720p.WEB.h264-BAE[TGx]
        "tt31193442", // Quiet on Set The Dark Side of Kids TV S01E01 Rising Stars Rising Questions 720p AMZN WEB-D...
        "tt6142940", // 90 Day Fiance Happily Ever After S08E01 Once Upon a Rocky Relationship 720p AMZN WEB-DL DD...
        "tt31530857", // Americas Backyard Gold S01E01 Rivers of Easy Gold 720p AMZN WEB-DL DDP2 0 H 264-NTb[TGx]
        "tt31186319", // Secret.World.of.Sound.with.David.Attenborough.S01E02.720p.x265-T0PAZ [Saturn5]
        "tt7235466", // 9-1-1.S07E01.720p.x264-FENiX [Saturn5]
        "tt10098248", // The Food That Built America S05E03 Soda Rising Birth of Pop 720p AMZN WEB-DL DDP2 0 H 264-...
        "tt0071030", // The Partridge Family, 2200 A.D (720p) {Plex101}
        "tt0413573", // Greys.Anatomy.S20E01.720p.x264-FENiX [Saturn5]
        "tt7053188", // Station.19.S07E01.720p.x264-FENiX [Saturn5]
        "tt10970364", // The Girls on the Bus S01E02 Shes With Her 720p HMAX WEB-DL DDP5 1 Atmos H 264-FLUX[TGx]
        "tt16912512", // Manhunt 2024 S01E01 Pilot 720p ATVP WEB-DL DDP5 1 Atmos H 264-FLUX[TGx]
        "tt14257126", // Challenger S01E02 HELP 720p NF WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt14371926", // Apples Never Fall S01E05 Troy 720p PCOK WEB-DL DDP5 1 H 264-FLUX[TGx]
        "tt12824922", // Long Way Up S01E05 Atacama Desert Into Bolivia 720p ATVP WEB-DL DDP5 1 H 264-NTb[TGx]
        "tt7678620", // Bluey.S03E47.ENG.720p.HD.WEBRip.112.20MiB.AAC.x264-PortalGoods
        "tt0382400", // Australian.Idol.S09E18.720p.HDTV.x264-ORENJI[TGx]
        "tt7749176", // Gold.Rush.White.Water.S00E14.In.Gold.We.Trust.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb[TGx]
        "tt13363298", // Trial.4.S01E07.Chapter.7.Black.Irish.720p.NF.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt31183637", // The.Program.Cons.Cults.and.Kidnapping.S01E01.Where.the.Fk.Am.I.720p.NF.WEB-DL.DDP5.1.H.264...
        "tt6439752", // Snowfall.S05E01.ENG.720p.HD.WEBRip.1.18GiB.AAC.x264-PortalGoods
        "tt7908628", // What.We.Do.in.the.Shadows.S05E10.Exit.Interview.720p.DSNP.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt26596318", // The.Family.Stallone.S02E04.Back.Aches.and.Blind.Dates.720p.AMZN.WEB-DL.DDP2.0.H.264-FLUX[T...
        "tt0495247", // The Apprentice (UK) You're Fired S18E06 Cereal (1280x720p HD, 50fps, soft Eng subs) ...
        "tt13362852", // Tiny.Toons.Looniversity.S00E01.Spring.Beak.720p.AMZN.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt1086761", // Keeping.Up.With.the.Kardashians.S19E07.Losing.it.in.Lockdown.720p.AMZN.WEB-DL.DDP5.1.H.264...
        "tt29794700", // Erika.Jayne.Bet.It.All.on.Blonde.S01E02.Part.Two.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb[TGx]
        "tt3850598", // BMF.S03E02.Magic.Makers.720p.AMZN.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt15822846", // The.Reluctant.Traveler.with.Eugene.Levy.S02E02.Scotland.My.Mothers.Country.720p.ATVP.WEB-D...
        "tt19516036", // The.Completely.Made-Up.Adventures.of.Dick.Turpin.S01E03.Run.Wilde.720p.ATVP.WEB-DL.DDP5.1....
        "tt0182576", // Family.Guy.S22E10.720p.x264-FENiX [Saturn5]
        "tt0239195", // Survivor.S46E02.720p.HDTV.x264-SYNCOPY[TGx]
        "tt3110590", // Car.SOS.S12E01.Ford.Pop.Hot.Rod.720p.HDTV.x264-skorpion.mkv
        "tt26246248", // Mary.and.George.S01E01.The.Second.Son.720p.NOW.WEB-DL.DDP5.1.H.264-FLUX[TGx]
        "tt2776888", // Big.Brother.Canada.S12E01.720p.AMZN.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt6156584", // Snowpiercer.S02E05.Keep.Hope.Alive.720p.BluRay.DD5.1.H.264-NTb[TGx]
        "tt21377088", // The.Enfield.Poltergeist.S01E01.The.Happenings.ATVP.WEB-DL.720p.AVC.EAC3.FLUX[TGx]
        "tt18699118", // Alert.Missing.Persons.Unit.S02E01.720p.HDTV.x265-MiNX[TGx]
        "tt10467076", // Shopping.with.Keith.Lemon.S04E08.Craig.Revel.Horwood.and.Fleur.East.720p.ITV.WEB-DL.AAC2.0...
        "tt18070984", // Average.Joe.S04E03.720p.WEB-DL.AAC2.0.H.264-NTb[TGx]
        "tt14236206", // ShAA.gun.2024.S01E03.Tomorrow.Is.Tomorrow.720p.DSNP.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt21375036", // The.Regime.S01E01.Victory.Day.REPACK.720p.AMZN.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt8211564", // Outback.Opal.Hunters.S11E08.720p.WEBRip.x264-skorpion.mp4
        "tt1610002", // Dog.Squad.S13E01.720p.TVNZ.WEB-DL.AAC2.0.H.264-NTb[TGx]
        "tt26748360", // Royal.Crackers.S02E01.Fight.for.J.Davis.High.720p.HMAX.WEB-DL.DD5.1.H.264-playWEB[TGx]
        "tt9068332", // Bless.the.Harts.S02E04.Dead.Mall.720p.HULU.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt31091758", // Married.to.the.Game.S01E01.Thats.the.Brutal.Thing.About.Football.720p.AMZN.WEB-DL.DDP5.1.H...
        "tt1827163", // Mob.Wives.S06E10.Whats.Done.Is.Done.720p.HULU.WEBRip.AAC2.0.H.264-NTb[TGx]
        "tt2874692", // When.Calls.The.Heart.S01E10.720p.WEB.H264-SKYFiRE[TGx]
        "tt8289930", // Formula.1.Drive.to.Survive.S06E03.Under.Pressure.720p.NF.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt11958610", // Trigger.Point.2022.S02E02.720p.WEB.x265-MiNX[TGx]
        "tt0139803", // (TOTP) Top of The Pops (Presented by Steve Wright) 4 Episodes (1980-1989) BBC4.720p.x264.d...
        "tt11774420", // Expedition.X.S07E02.Killer.Sasquatch.720p.MAX.WEB-DL.DD.2.0.H.264-playWEB[TGx]
        "tt9859436", // The.Walking.Dead.The.Ones.Who.Live.S01E01.720p.x265-T0PAZ [Saturn5]
        "tt0312172", // Monk.S04E11.Mr.Monk.Bumps.His.Head.4K.Remaster.720p.BluRay.FLAC2.0.H.264-NTb[TGx]
        "tt2049323", // The.Jonathan.Ross.Show.S21E02.720p.HDTV.x264-NGP[TGx]
        "tt0094415", // Americas.Most.Wanted.S27E02.720p.WEB.h264-DiRT[TGx]
        "tt27954448", // Law.and.Order.Toronto.Criminal.Intent.S01E01.720p.HDTV.x264-SYNCOPY[TGx]
        "tt19395018", // Constellation.S01E02.Live.And.Let.Die.720p.ATVP.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt0344651", // Expats.S01E06.Home.720p.AMZN.WEB-DL.DDP5.1.H.264-CMRG[TGx]
        "tt11828386", // The.Second.Best.Hospital.in.the.Galaxy.S01E07.Just.One.More.Adjustment.720p.AMZN.WEB-DL.DD...
        "tt6645582", // Summer.House.S08E01.Declaration.of.Codependence.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb[TGx]
        "tt9111220", // Hudson.and.Rex.S06E07.720p.HDTV.x265-MiNX[TGx]
        "tt17543592", // Will.Trent.S02E01.720p.HDTV.x264-SYNCOPY[TGx]
        "tt6470478", // The.Good.Doctor.S07E01.Baby.Baby.Baby.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb[TGx]
        "tt0096563", // Cops.S35E27.720p.WEB.h264-BAE[TGx]
        "tt3530232", // Last.Week.Tonight.with.John.Oliver.S11E01.February.18.2024.720p.AMZN.WEB-DL.DDP2.0.H.264-N...
        "tt6186672", // Aussie.Gold.Hunters.S09E01.720p.WEBRip.x264-skorpion.mp4
        "tt11242246", // The.Equalizer.2021.S04E01.720p.x265-T0PAZ [Saturn5]
        "tt11809048", // The.Equalizer.2021.S04E01.Truth.for.a.Truth.720p.AMZN.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt0096697", // The.Simpsons.S35E11.720p.x265-T0PAZ [Saturn5]
        "tt12887536", // CSI.Vegas.S03E01.720p.x264-FENiX [Saturn5]
        "tt11704040", // Love.Is.Blind.S06E06.NORDiC.720p.WEBRip.x264-STATiXDK
        "tt16098700", // Fire.Country.S02E01.720p.x264-FENiX [Saturn5]
        "tt11379026", // Ghosts.US.S03E01.The.Owl.720p.AMZN.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt18399598", // Why.the.Heck.Did.I.Buy.This.House.S02E01.Relaxed.Rancher.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb...
        "tt5737432", // The.Nineties.S01E01.The.One.About.TV.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb[TGx]
        "tt18573724", // So.Help.Me.Todd.S02E01.720p.x264-FENiX [Saturn5]
        "tt6226232", // Young.Sheldon.S07E01.720p.x265-T0PAZ [Saturn5]
        "tt8690918", // Resident.Alien.S03E01.720p.x264-FENiX [Saturn5]
        "tt8595140", // The.Conners.S06E02.720p.x264-FENiX [Saturn5]
        "tt1984119", // Feud.S02E04.720p.WEB.x265-MiNX[TGx]
        "tt9742936", // FBI.Most.Wanted.S05E01.720p.x264-FENiX [Saturn5]
        "tt18177528", // The.New.Look.S01E01.720p.WEB.x265-MiNX[TGx]
        "tt7942794", // The.Neighborhood.S06E01.720p.x265-T0PAZ [Saturn5]
        "tt29815504", // Allegiance.2024.S01E01.720p.WEBRip.x265-MiNX[TGx]
        "tt14218674", // NCIS.Hawaii.S03E01.720p.x265-T0PAZ [Saturn5]
        "tt13684264", // Here.We.Go-Mums.Classic.Family.Christmas--2023-BBC-720p-w.subs-265-HEVC.mkv
        "tt11011104", // Game.Changer.S06E01.Second.Place.720p.WEB-DL.Opus2.0.H.264-.NTb[TGx]
        "tt10329024", // Bob.Hearts.Abishola.S05E01.The.Dead.Eyes.of.a.Respectful.Son.720p.AMZN.WEB-DL.DDP5.1.H.264...
        "tt2100976", // Impractical.Jokers.S10E11.720p.WEB.h264-BAE[TGx]
        "tt18250904", // Not.Dead.Yet.S02E01.720p.x265-T0PAZ [Saturn5]
        "tt7686456", // Jersey.Shore.Family.Vacation.S07E01.720p.WEB.h264-BAE[TGx]
        "tt3450552", // The.Happenings.S01E01.Sixth.Sense.720p.HDTV.x264-DHD
        "tt14218830", // Abbott.Elementary.S03E01.720p.x264-FENiX
        "tt2342499", // Below.Deck.S11E01.720p.WEB.h264-EDITH[TGx]
        "tt1888075", // Death.In.Paradise.S13E01.720p.HDTV.x264-ORGANiC[TGx]
        "tt14028208", // Clone.High.2023.S02E08.720p.WEB.H264-RABiDS[TGx]
        "tt0264235", // Curb.Your.Enthusiasm.S12E01.Atlanta.720p.MAX.WEB-DL.DDP5.1.x264-NTb[TGx]
        "tt2343157", // Vanderpump.Rules.S11E01.Notes.On.A.Scandal.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb[TGx]
        "tt3781836", // Botched.S08E13.Big.Booty.Queen.720p.AMZN.WEB-DL.DDP2.0.H.264-NTb[TGx]
        "tt27621450", // Legion.S01E04.720p.BluRay.DD5.1.x264-NTb[TGx]
        "tt8773420", // Expats.S01E03.Mid.Levels.720p.AMZN.WEB-DL.DDP5.1.H.264-CMRG[TGx]
        "tt2640044", // Masters.of.the.Air.S01E03.720p.WEB.x265-MiNX[TGx]
        "tt0206501", // NOVA.S51E01.When.Whales.Could.Walk.720p.x264-FENiX [Saturn5]
        "tt4771108", // Married.At.First.Sight.S17E00.Afterparty.New.Groom.Who.Dis.720p.WEB.h264-EDITH[TGx]
        "tt18259204", // Sexy.Beast.S01E04.Always.Wanted.To.See.That.Place.720p.AMZN.WEB-DL.DDP5.1.H.264-NTb[TGx]
        "tt17043230", // Quantum.Leap.2022.S02E09.720p.x265-T0PAZ [Saturn5]
        "tt0133302", // Frontline.S42E02.Democracy.on.Trial.720p.WEB.h264-BAE[TGx]
        "tt30487848", // Sight.Unseen.2024.S01E02.720p.WEBRip.x264-BAE[TGx]
        "tt11312248", // Extended.Family.S01E06.720p.HDTV.x265-MiNX[TGx]
        "tt16288838", // The.Irrational.S01E08.720p.x264-FENiX [Saturn5]
        "tt13911628", // Lessons.in.Chemistry.S01E06.720p.WEB.h264-EDITH[TGx]
        "tt13366604", // Platonic.2023.S01E09.720p.WEB.h264-EDITH[TGx]
        "tt23033802", // Still.Up.S01E03.720p.WEB.h264-EDITH[TGx]
        "tt8559532", // The.Changeling.S01E05.720p.WEB.h264-EDITH[TGx]
        "tt14914464", // Strange.Planet.S01E09.720p.WEB.h264-EDITH[TGx]
        "tt0313038", // The.Bachelor.S28E02.720p.WEB.h264-EDITH[TGx]
        "tt7203552", // The.Morning.Show.2019.S03E04.720p.WEB.h264-EDITH[TGx]
        "tt10986410", // Ted.Lasso.S03E12.720p.WEB.h264-EDITH[TGx]
        "tt0455275", // Prison Break S02E07 720p [Timati]
        "tt15051378", // I Literally Just Told You S03E01
        "tt0380136", // QI XL S21E04 - Unsavoury
        "tt7316998", // Richard Osman's House of Games S07E70 - Week 14 Day 5 (x265)
        "tt30588907", // Jeopardy! (UK) S01E15 (subs x265)
        "tt30887606", // After.Midnight.2024.2024.01.18.Max.Greenfield.1080p.WEB.h264-EDITH
        "tt21822590", // The.Woman.in.the.Wall.S01E01.1080p.WEB.H264-DeviousDramaticQuailOfStorm[TGx]
        "tt15499118", // The Madame Blanc Mysteries S03E03
        "tt30887599", // After.Midnight.2024.2024.01.17.Paul.F.Tompkins.1080p.WEB.h264-EDITH
        "tt2337840", // Bad.Education-A.Christmas.Carol--2023-BBC-720p-w.subs-x265-HEVC.mkv
        "tt30495659", // The.Cambridgeshire.Crucifixion.2024.1080p.WEBRip.x264-CBFM[TGx]
        "tt10714820", // Best.Interests.S01E04.1080p.WEBRip.x264-CBFM[TGx]
        "tt30836097", // 19th Floor (Chinese 19层 / 19th Ceng) - 2024
        "tt14449470", // FBI.International.S02E18.Debito.di.sangue.DLMux.1080p.x264.AC3.ITA-ENG.Sub.ENG.by.quintrix...
        "tt7491982", // FBI.S05E19.Errori.del.passato.DLMux.1080p.x264.AC3.ITA-ENG.Sub.ENG.by.quintrix.mkv
        "tt28254776", // Raid.the.Cage.S01E12.1080p.WEB-DL.H264-MassModz
        "tt26450613", // A Shop for Killers (Korean: 킬러들의 쇼핑몰) - 2024
        "tt11129816", // Malory.Towers
        "tt0052520", // The.Twilight.Zone.
        "tt0068069", // Emmerdale
        "tt26653824", // Welcome to Samdal-ri
        "tt0053494", // Coronation street
        "tt4653556", // Danny.And.The.Human.Zoo.
        "tt2125666", // Queen.of.Versailles
        "tt26442984", // The.Magical.World.of.Moss
        "tt12776736", // This.World.Stacey.Dooley.Inside.Spains.Narco.Wars
        "tt12989852", // Sea.Lions.Life.By.A.Whisker
        "tt5938484", // Homestead.Rescue.
        "tt3204810", // On.Cinema
        "tt0199257", // Panorama
        "tt1091909", // Murdoch.Mysteries.
        "tt20885036", // Young.David
        "tt0098740", // Americas.Funniest.Home.Videos
        "tt0123338", // 60.Minutes
        "tt5212822", // Imposters
        "tt11857512", // Crime.Beat
        "tt13315272", // Escape.to.the.Farm.with.Kate.Humble.
        "tt0096555", // Casualty
        "tt1055238", // Would.I.Lie.to.You
        "tt1190689", // Nurse.Jackie
        "tt2182427", // Bering.Sea.Gold.
        "tt0063951", // Sesame.Street
        "tt13623608", // The.Curse
        "tt7772588", // For.All.Mankind
        "tt0103396", // Dateline.NBC.S32E04.WEB
        "tt1720601", // The.Real.Housewives.of.Beverly.Hills
        "tt0486531", // Independent.Lens
        "tt2326517", // Finding.Your.Roots.
        "tt10732048", // Three.Women.
        "tt0487189", // Waterloo.Road
        "tt6906110", // Maine.Cabin.Masters
        "tt0200325", // Antiques.Roadshow.US.
        "tt15791752", // Grimsburg.S01E01
        "tt29255614", // Buddy.Valastros.Cake.Dynasty
        "tt9140632", // The.Great.North
        "tt1561755", // Bobs.Burgers
        "tt9516466", // Ambulance.Australia
        "tt3475084", // The.Kitchen
        "tt5117072", // Apres.Ski.S01E01
        "tt14075028", // Jessicas.Big.Little.World
        "tt12759778", // Irvine.Welshs.Crime
        "tt15395964", // Coach.Prime
        "tt1715425", // Digging.for.Britain
        "tt21047514", // Make.Some.Noise
        "tt20883058", // On.Patrol.Live
        "tt0088512", // Eastenders.2024
        "tt7263064", // Hot.Rod.Garage
        "tt14016574", // Krapopolis.
        "tt27238424", // The.Whole.Story.With.Anderson.Cooper.
        "tt15439048", // Death.and.Other.Details
        "tt10338160", // 'Chad'
        "tt0169446", // The.Fifth.Estate.S49E08.WEB.x264-TORRENTGALAXY
        "tt1800864", //Gold.Rush.S14E17.WEB.x264-TORRENTGALAXY
        "tt28959685", // Love.Island.All.Stars.
        "tt10477884", // The.Masked.Singer.UK
        "tt8050756", // The.Owl.House
        "tt0203259", // Law.and.Order.Special.Victims.Unit
        "tt13070378", // Worlds.Funniest.Animals
        "tt1811179", // Penn.and.Teller.Fool.Us
        "tt0124932", // 20-20
        "tt1442550", // Shark.Tank.
        "tt1353056", // RuPauls.Drag.Race
        "tt0350448", // Real.Time.with.Bill.Maher
        "tt10936342", // Transplant,
        "tt2345459", // 8.Out.of.10.Cats.Does.Countdown
        "tt2057880", // Watch.What.Happens.Live
        "tt1693592", // Vera
        "tt5327970", // The.Real.Housewives.of.Potomac.
        "tt20221678", // The.Way.Home
        "tt29218693", // Baddies.East
        "tt2543378", // Married.to.Medicine
        "tt20918830", // Bollywed
        "tt3469050", // 90.Day.Fiance
        "tt6666966", // Home.Town
        "tt11396164", // Rich.Holiday.Poor.Holiday
        "tt6417190", // The.Great.Pottery.Throw.Down
        "tt0499383", // Dancing.on.Ice.
        "tt1983079", // Call.The.Midwife.
        "tt4859164", // The.Loud.House
        "tt14877120", // Kiff
        "tt2267661", // Pointless.Celebrities
        "tt0072562", // Saturday.Night.Live
        "tt23931190", // Belgravia.The.Next.Chapter
        "tt14203572", // Monsieur.Spade.
        "tt0072562", // Saturday.Night.Live
        "tt30490874", // The.Traitors.Uncloaked
        "tt2271773", // Winterwatch.
        "tt23743442", // The.Traitors.UK.
        "tt0924651", // The.Graham.Norton.Show.
        "tt20252368", // Zorro.2024
        "tt0112004", // Hollyoaks 19th Jan 2024
        "tt0098844", // Law.and.Order.
        "tt12677870", // Law.and.Order.Organized.Crime
        "tt0320037", // Jimmy.Kimmel
        "tt3513388", // Seth.Meyers
        "tt3444938", // Jimmy.Fallon
        "tt21371228", // Ruby.Speaking
        "tt21371338", // Count.Abdulla.
        "tt0213373", // Tonight.
        "tt0115355", // Silent.Witness.
        "tt10038600", // Press.Your.Luck
        "tt3697842", // Stephen.Colbert
        "tt0437005", // Hells.Kitchen
        "tt2815522", // Southern.Charm
        "tt20222554", // Southern.Hospitality.
        "tt11761176", // Power.Book.III
        "tt22353310", // Prosper
        "tt13554536", // Late.Bloomer.
        "tt15557874", // The.Traitors
        "tt7216636", // Hazbin.Hotel.S01E01.
        "tt21632986", // Sanctuary.A.Witchs.Tale
        "tt0083452", // Nature.
        "tt0443370", // Dragons.Den.CA
        "tt11784676", // Double.Cross
        "tt10752770", // Tyler.Perrys.Sistas.
        "tt8594324", // Ghosts
        "tt20195430", // Hush
        "tt3868860", // Married.At.First.Sight
        "tt0176095", // The.Challenge.
        "tt1833558", // The.Real.Housewives.of.Miami
        "tt21840464", // No.Gamble.No.Future
        "tt0163963", // This.Hour.Has.22
        "tt11852790", // Moonshiners.Master.Distiller
        "tt0446622", // Hard.Knocks
        "tt11640018", // La.Brea
        "tt11363282", // The.Real.Housewives.of.Salt.Lake.City
        "tt9071808", // Found.2023
        "tt13798316", // Night.Court.2023
        "tt3455408", // The.Curse.of.Oak.Island.
        "tt14456760", // Run.the.Burb
        "tt15310816", // Son.of.a.Critch.
        "tt10684374", // The.Ghost.and.Molly.McGee.
        "tt0177465", // University.Challenge
        "tt2224452", // Love.and.Hip.Hop.Atlanta
        "tt7820906", // Good.Trouble
        "tt10580092", // Tyler.Perrys.The.Oval
        "tt2795196", // Ben.Fogle.New.Lives.
        "tt2980110", // First.Dates
        "tt4903514", // Inside.The.Factory
        "tt13491734", // Name.That.Tune.
        "tt21088136", // Criminal.Record.
        "tt3713810", // GPs.Behind.Closed.Doors.
        "tt14599926", // Hollywood.Houselift.with.Jeff.Lewis
        "tt13990474", // 90.Day.The.Single.Life
        "tt1991410", // Dance.Moms.
        "tt2390276", // Highway.Thru.Hell.
        "tt13787288", // 90.Day.Diaries
        "tt6293468", // All.New.Traffic.Cops.
        "tt0159847", // Antiques.Roadshow
        "tt0795174", // POV.
        "tt5665418", // Below.Deck.Mediterranean
        "tt1637756", // Basketball.Wives
    ];

    const execludeNames = [
        "Extraordinary.Extensions.",
        "22.Kids.and.Counting",
        "Wilderness.with.Simon.Reeve.",
        "George.Clarkes.Adventures.in.America",
        "Pointless.S\\d+",
        "Jimmy.Carrs.I.Literally.Just.Told.You.S\\d+",
        "Gladiators.UK.2024.S\\d+",
        "The.Masked.Singer.",
        "Big.Zuus",
        "Christopher.Kimballs.Milk.Street.Television",
        "The.Canary.Islands.with.Jane.McDonald",
        "Cruising.with.Susan.Calman",
        "Ancient.Aliens",
    ];


    const paginationDivs = document.querySelectorAll('.pagination');
    const lastPaginationDiv = paginationDivs.length == 0 ? null : paginationDivs.item(paginationDivs.lenght-1);
    console.warn(lastPaginationDiv);

    const exclTableContainer = document.createElement("div");
    exclTableContainer.setAttribute("class", "container-fluid");
    lastPaginationDiv.parentElement.parentElement.parentElement.appendChild(exclTableContainer);

    const exclTable = document.createElement("div");
    exclTable.setAttribute("class", "tgxtable");
    exclTable.style.opacity = 0.6;
    exclTableContainer.appendChild(exclTable);

    const exclTableHeader = document.querySelector(".tgxtablehead").cloneNode(true);
    exclTable.appendChild(exclTableHeader);

    const tableRows = document.querySelectorAll(".tgxtablerow");
    console.warn("TABLE ROW CNT: " + tableRows.length);
    for(let i=0;i<tableRows.length; i++) {
        const row = tableRows.item(i);

        let torrentName = "";
        const dataHrefDiv = row.querySelector('[data-href]');
        if(dataHrefDiv) {
            jQuery(dataHrefDiv).off("click");
            torrentName = dataHrefDiv
        }

        const torrentSrcSpan = row.querySelector('span[src="torrent"]');
        //console.warn("TORRENT NAME: " + torrentSrcSpan.innerText);

        let imdbId = "---";
        const imdbSearchImg = row.querySelector('img[title="IMDB search"');
        if(imdbSearchImg) {
            const imdbLinkA = imdbSearchImg.parentElement;
            if(imdbLinkA) {
                // console.warn(imdbLinkA.href);
                imdbId = /^.*(tt\d+)$/.exec(imdbLinkA.href)[1];
            }

            if(torrentSrcSpan) {
                const clipboardButton = document.createElement('button');
                clipboardButton.setAttribute('value', 'clipboard');
                clipboardButton.setAttribute("class", "clipboardButton_"+imdbId+"_"+i);
                clipboardButton.innerText = ">>Copy<<";
                clipboardButton.style.fontSize = "8px";
                clipboardButton.style.padding = "0px 2px";
                clipboardButton.style.lineHeight = "8px";
                //clipboardButton.setAttribute("hidden", "true");
                torrentSrcSpan.parentElement.parentElement.appendChild(clipboardButton);
                new ClipboardJS('.clipboardButton_'+imdbId+"_"+i, {
                    text: function() {
                        const txt = '"' + imdbId + '", // ' + torrentSrcSpan.innerText;
                        console.warn("Copied to clipboard: " + txt);
                        return txt;
                    }
                });
            }
        }
        //console.warn("IMDB ID: " + imdbId);

        let categoryTag = row.querySelector('[href*="/torrents.php?cat=41"]'); // TV : Episodes HD
        if(!categoryTag) {
            categoryTag = row.querySelector('[href*="/torrents.php?cat=5"]'); // TV : Episodes SD
        }
        //console.warn("Category: ");console.warn(categoryTag);

        let langImg = row.querySelector('img[src^="/common/images/languages/english.jpg"]');
        if(!langImg) {
            langImg = row.querySelector('img[src^="/common/images/languages/other.jpg"]');
        }

        if( !categoryTag
           || imdbId == "---" || excludes.includes(imdbId)
           || torrentSrcSpan.innerText.search(/(720p|XviD-AFG)/) < 0
           || execludeNames.some(ex => {
            //console.warn("REGEX EX: .*" + ex +".*");
            return new RegExp(ex + ".*", 'g').test(torrentSrcSpan.innerText);
        })
           || !langImg
          ) {
            row.style.zoom = 0.8;
            //jQuery(row).hide();
            row.remove();
            exclTable.appendChild(row);

        }

    }


})();