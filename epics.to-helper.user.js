// ==UserScript==
// @name         epics.to-helper
// @namespace    -forums-
// @version      0.1
// @description
// @author       SzF
// @match        https://eropics.to/20*
// @grant        GM_registerMenuCommand
// @grant        GM_addStyle
// @downloadURL  https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/epics.to-helper.user.js?inline=false
// @updateURL    https://gitlab.com/fszirbik/google-chrome-scripts/-/raw/master/epics.to-helper.user.js?inline=false
// ==/UserScript==

(function() {
    const start = Date.now();

    'use strict';

    const prefixExcludes = [
        "jada420", "Anilos Helen", "ATKHairy", "TessaFowler", "Asian4You", "TheBlackAlley", "ATKExotics", "College-Uniform", "MilenaAngel",
        "EviLAngel", "EveAngel", "Red-XXX", "AbbyWinters", "GirlsWay", "Nadine-J", "OfficialSammyBraddy", "SkinTightGlam", "Scoreland", "ThisIsGlam",
        "Rin-City", "Stylerotica", "ForbiddenRealm", "Octokuro", "StaceyPoole", "BurningAngel", "Anilos", "Dominika-C", "Sexy-Lena", "LovePop",
        "Katya-Clover", "Brittany-Marie", "TheEmilyBloom", "MomsLickTeens", "EuroGirlsOnGirls", "AlettaOcean", "HotAndMean", "CandiceBrielle",
        "LethalHardcore", "WhenGirlsPlay", "Girlz-High", "Digi-Gra", "PornstarPlatinum", "ArielRebel", "AuntJudy", "Cosmid", "ATKingdom", "HotLegsAndFeet",
        "NancyAce", "MomKnows", "SatinPlay", "Vintage", "21Sextreme", "SimplyAnal", "Gravure", "LeanneCrow", "BabeChannel", "Mommys", "Xiuren", "AllGravure",
        "MoreThanNylons", "Foxes", "BootyLicious", "EliteTVOnline", "NothingButCurves",
// Met-Art
        "MetArt Sade Mare", "MetArt Katherine A",
// Nubiles
        "Nubiles Marta aka Melena",
    ].map(excl => excl.toLowerCase()).sort();


    const dayMatch = /^\/(\d{4})\/(\d{2})\/(\d{2})((\/page\/(\d+))|.*)?$/.exec(window.location.pathname);
    console.warn("DAY MATCH");
    console.warn(dayMatch);
    if(!dayMatch) {
        return;
    }

    const day = dayMatch[1] + "-" + dayMatch[2] + "-" + dayMatch[3];
    // console.warn("epics.to start: " + day);
    const galleryPage = dayMatch[4] && !/^\/page\/.+/.test(dayMatch[4]);
    const pageNum = dayMatch[6];

    if(galleryPage) {
        console.warn("GALLERY PAGE");

        const articleTag = document.querySelector("article");
        console.warn("article");
        //console.warn(articleTag);

        const entryContentTag = articleTag.querySelector("div.entry-content");
        console.warn("entry-content");
        //console.warn(entryContentTag);

        const items = entryContentTag.querySelectorAll("a");
        console.warn("items");
        //console.warn(items);

        const min_image_count = 10;
        if (items.length > min_image_count) {
            for(let i = 0; i<items.length; i++) {
                //console.warn(items[i]);
                const img = items[i].querySelector("img");
                if(!img) continue;
                img.setAttribute('data-src', img.getAttribute("src"));

                let num = Math.floor(i % (items.length / min_image_count));
                if(num !== 0) {
                    items[i].setAttribute("hidden", true);
                    img.setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs='); /*empty GIF*/
                }
            }

            const entryHeaderTag = articleTag.querySelector("header.entry-header");

            const catLinksTags = articleTag.querySelectorAll(".cat-links");
            catLinksTags.forEach(catLinksTag => {
                var allButton = document.createElement('button');
                allButton.setAttribute('style', 'position: absolute; right: 0; left: auto;');
                allButton.innerHTML = 'ALL';
                allButton.addEventListener('click', function (event) {
                    if(allButton.innerHTML === 'HIDE') {
                        allButton.innerHTML = 'ALL';
                        for(let i = 0; i<items.length; i++) {
                            //console.warn(items[i]);
                            const img = items[i].querySelector("img");
                            img.setAttribute('data-src', img.getAttribute("src"));

                            let num = Math.floor(i % (items.length / min_image_count));
                            if(num !== 0) {
                                items[i].setAttribute("hidden", true);
                                img.setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs='); /*empty GIF*/
                            }
                        }
                    } else {
                        allButton.innerHTML = 'HIDE';
                        for(let i = 0; i<items.length; i++) {
                            items[i].removeAttribute("hidden");
                            const img = items[i].querySelector("img");
                            img.setAttribute('src', img.getAttribute("data-src"));
                        }
                    }
                });
                catLinksTag.appendChild(allButton);
            });
        }
        return;
    }

    const titleTag = document.querySelector("title");
    const title = "Eropics - " + day + (pageNum ? " ["+pageNum+"]" : "");
    titleTag.textContent = title;
    //console.warn("epics.to title: " + titleTag.textContent);


    function addDays(date, days) {
        return new Date(date.getTime() + (days * 86400000));
    }

    function checkPrevPage(date) {
        checkPage(date, -1);
    }
    function checkNextPage(date) {
        checkPage(date, 1);
    }

    var checkCnt = 0;
    function checkPage(date, direction) {
        if(checkCnt++ >= 14) {
            console.error("CHECK NEXT FAILED: NO PORE PAGES");
            checkCnt = 0;
            return;
        }

        if(!direction || direction < 0) {
            direction = -1;
        } else {
            direction = 1;
        }

        date = date.toISOString().replaceAll(/T.*$/g, "");
        // console.warn("CHECK DIRECTION: " + direction);
        console.warn("CHECK DATE: " + date);

        titleTag.textContent = title + " >>"+checkCnt+"<<";
        const prevPageURL = "/" + date.replaceAll("-", "/");

        const req = new XMLHttpRequest();
        req.addEventListener("load", () => {
            console.warn("CHECK NEXT LOADED");
            // console.warn(req);
            if(req.status == 200) {
                // console.warn("CHECK NEXT OK");
                setTimeout(() => location.assign(prevPageURL), 1000);
            } else if(req.status == 404) {
                // console.warn("CHECK NEXT 404");
                const newDate = addDays(new Date(date), direction);
                setTimeout(() => checkPage(newDate, direction), 1000);
            } else {
                console.error("CHECK NEXT FAILED" + req.status + " " + req.statusText);
                alert("No more pages.");
            }
        });
        req.open("GET", prevPageURL);
        req.send();
    }

    const paginationTag = document.querySelector("ul.pagination");
    // console.warn("epics.to pagination: " + paginationTag);
    let paginationPrevTag = null;
    let paginationNextTag = null;
    if(paginationTag) {
        paginationPrevTag = paginationTag.querySelector(".prev");
        paginationNextTag = paginationTag.querySelector(".next");
    }
    // console.warn("epics.to pagination-next: " + (paginationNextTag ? paginationNextTag.getAttribute("href") : null));
    // console.warn("epics.to pagination-prev: " + (paginationPrevTag ? paginationPrevTag.getAttribute("href") : null));

    document.onkeydown = function (evt) {
        evt = evt || window.event;
        //console.log(evt);
        //console.log(evt.ctrlKey);
        //console.log(evt.key);
        //console.log(evt.keyCode === 37);
        if (evt.ctrlKey === true && evt.keyCode === 37) { // LEFT
            if(paginationPrevTag) {
                console.warn(paginationPrevTag.getAttribute('href'));
                evt.stopPropagation();
                evt.preventDefault();
                location.assign(paginationPrevTag.getAttribute('href'));
//            } else {
//                let prevDate = new Date(day);
//                prevDate.setDate(prevDate.getDate() - 1);
//                checkPrevPage(prevDate);
            }
        }
        if (evt.ctrlKey === true && evt.keyCode === 39) { // RIGTH
            if(paginationNextTag) {
                // console.warn(paginationNextTag.getAttribute('href'));
                evt.stopPropagation();
                evt.preventDefault();
                location.assign(paginationNextTag.getAttribute('href'));
            }else {
                const nextDate = addDays(new Date(day), 1);
                checkNextPage(nextDate);
            }
        }
    };

    const siteMainTag = document.querySelector("main.site-main");
    const items = siteMainTag.querySelectorAll("article.post");
    for(let i=0; i<items.length; i++) {
        const entryTitleTag = items[i].querySelector(".entry-title");
        const entryTitleATag = entryTitleTag.querySelector("a");
        if(prefixExcludes.find(pe => entryTitleATag.textContent.toLowerCase().startsWith(pe))) {
            items[i].setAttribute("style", "opacity: 20%;");
            //items[i].setAttribute("hidden", "true");
        }

    }

})();
